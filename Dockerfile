FROM ros:iron

# defines the default directory for building micro-ros
ENV MICROROS_DIR=/microros_ws
ENV TOOLCHAIN=/armr5-toolchain

# ------------------------------------------------------------------------------
# Base programs
RUN apt-get update
RUN apt-get dist-upgrade -y
RUN apt-get install -y \
    python3-pip \
    cmake \
    curl \
    wget

# ------------------------------------------------------------------------------
# Set and install ROS dependancies and environment
WORKDIR $MICROROS_DIR

# Obtain the micro-ros sources
RUN git clone -b $ROS_DISTRO https://github.com/micro-ROS/micro_ros_setup.git \
    $MICROROS_DIR/src/micro_ros_setup

RUN rosdep fix-permissions
RUN rosdep update
RUN rosdep install --from-paths src --ignore-src -y

RUN /bin/bash -c 'source /opt/ros/$ROS_DISTRO/setup.bash && \
    colcon build'

# ------------------------------------------------------------------------------
# Obtain compiler for the KRIA board
RUN wget "https://developer.arm.com/-/media/Files/downloads/\
gnu/12.2.mpacbti-rel1/binrel/arm-gnu-toolchain-12.2\
.mpacbti-rel1-x86_64-arm-none-eabi.tar.xz"
RUN mkdir $TOOLCHAIN
RUN tar -xvf ./arm-gnu-toolchain-12.2.mpacbti-rel1-x86_64-arm-none-eabi.tar.xz \
    -C $TOOLCHAIN --strip-components=1
RUN rm -f arm-gnu-toolchain-12.2.mpacbti-rel1-x86_64-arm-none-eabi.tar.xz
ENV PATH="${PATH}:$TOOLCHAIN/bin"

# ------------------------------------------------------------------------------
# Setting the sources to be able to use the container as a build environment
RUN echo ". /opt/ros/$ROS_DISTRO/setup.bash" >> /root/.bashrc
RUN echo ". $MICROROS_DIR/install/local_setup.bash" >> /root/.bashrc
