#+title: libmicroros Docker

A Dockerfile setup to build a container ready to create your ~libmicroros.a~ library.

* Building the container

#+BEGIN_SRC sh
  docker build -t sunoc/microros_build .
#+END_SRC


* Downloading from [[https://hub.docker.com/][Dockerhub]]
The container can be downloaded from [[https://hub.docker.com/repository/docker/sunoc/microros_build/general][my Dockerhub page]] as follow:

#+BEGIN_SRC sh
  docker run -d --name microros -it --net=host \
         --hostname microros_docker -v /dev:/dev \
         --privileged sunoc/microros_build
#+END_SRC

Then, one can enter the container with a bash shell:

#+BEGIN_SRC sh
  docker exec -it microros bash
#+END_SRC

